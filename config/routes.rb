Rails.application.routes.draw do
  # root 'devise/sessions/new'
  devise_scope :user do
    root to: "devise/sessions#new"
  end

  devise_for :users, controllers: { sessions: 'users/sessions' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
